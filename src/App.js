//import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from './Pages/Home';
import Contact from './Pages/Contact';
import Aboutus from './Pages/Aboutus';

function App() {
  return (
    <Router>
    <Route path='/' exact component={Home} />
    <Route path='/Contact' exact component={Contact} />
    <Route path='/Aboutus' exact component={Aboutus} />
    </Router>
  );
}

export default App;
