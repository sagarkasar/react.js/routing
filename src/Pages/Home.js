import React from 'react';
import '../Assets/Comman.css';
import { Container, Grid, Paper} from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { Typography } from '@material-ui/core';
import TopBar from './TopBar';
export default function Home(){
    
    const history = useHistory();
    
    return(
        <div className='maindiv' spacing={2}>
        <TopBar/>
        <Container>
        <Grid item sm={12} spacing={2}>
        <Typography className='mainhead'>
         Welcome To Home
        </Typography>
        <Grid className='gridpaper'>
        <Grid container item item sm={3} className='griddiv'>
        <Paper elevation={3} className='paperdiv'>
        hii
        </Paper>
        </Grid>
        <Grid container item item sm={3} className='griddiv'>
        <Paper elevation={3} className='paperdiv'>
         hello
        </Paper>
        </Grid>
        <Grid container item item sm={3} className='griddiv'>
        <Paper elevation={3} className='paperdiv'>
         welcome 
        </Paper>
        </Grid>
        <Grid container item item sm={3} className='griddiv'>
        <Paper elevation={3} className='paperdiv'>
         welcome 
        </Paper>
        </Grid>
        </Grid>
        </Grid>
        </Container>
        </div>
    );
}