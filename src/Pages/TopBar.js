import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { useHistory } from 'react-router-dom';

export default function TopBar(){
    const history = useHistory();
    return(
        <div className='TopbarDiv'>
            <AppBar position="static">
            <Toolbar>
            <IconButton edge="start" className='menuButton' color="inherit" aria-label="menu">
                <MenuIcon />
            </IconButton>
            <Typography variant="h6" className='title'>
                News
            </Typography>
            <div className='btndiv'>
            <Button variant="outlined" color="secondary" onClick={() => history.push('/')}>Home</Button>
            <Button variant="contained" color="primary" onClick={() => history.push('/Contact')}>Contact</Button>
            <Button variant="contained" color="primary" onClick={() => history.push('/Aboutus')}>About Us</Button>
            <Button variant="contained" color="secondary">Login</Button>
            </div>
            </Toolbar>
            </AppBar>
        </div>
    )
}